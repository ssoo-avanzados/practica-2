/*
    sim_pag_lru.c
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "sim_paginacion.h"

// Función que inicia las tablas

void iniciar_tablas (ssistema * S)
{
    int i;

    // Páginas a cero
    memset (S->tdp, 0, sizeof(spagina)*S->numpags);

    // Pila LRU vacía
    S->lru = -1;

    // Tiempo LRU(t) a cero
    S->reloj = 0;

    // Lista circular de marcos libres
    for (i=0; i<S->nummarcos-1; i++)
    {
        S->tdm[i].pagina = -1;
        S->tdm[i].sig = i+1;
    }

    S->tdm[i].pagina = -1;  // Ahora i == nummarcos-1
    S->tdm[i].sig = 0;      // Cerrar lista circular
    S->listalibres = i;     // Apuntar al último

    // Lista circular de marcos ocupados vacía
    S->listaocupados = -1;
}

// Funciones que simulan el hardware de la MMU

unsigned sim_mmu (ssistema * S, unsigned dir_virtual, char op)
{
    unsigned dir_fisica;
    int pagina, marco, desplazamiento;

    pagina = dir_virtual / S->tampag;                           //Cociente
    desplazamiento = dir_virtual % S->tampag;                   //Resto

    if(pagina<0 || pagina>=S->numpags)
    {
        S->numrefsilegales ++;
        return ~0U;                                             //Devolver la dir. fisica FF...F
    }

    if(!S->tdp[pagina].presente)                                //Si no está presente
    {
        tratar_fallo_de_pagina(S,dir_virtual);                  //Fallo de pagina
    }

    marco = S -> tdp [ pagina ]. marco ;                        //Calcular direccion
    dir_fisica = marco * S->tampag + desplazamiento;            //fisica
    
    referenciar_pagina (S , pagina , op );                      //Referenciar la pagina

    if (S -> detallado )
    {
        printf ("\t %c %u == P %d(M %d)+ %d\n" ,op , dir_virtual , pagina , marco , desplazamiento );
    }
    return dir_fisica;
}

void referenciar_pagina (ssistema * S, int pagina, char op)
{
    if (op=='L')                         // Si es una lectura,
        S->numrefslectura ++;            // contarla
    else if (op=='E')
    {                                    // Si es una escritura,
        S->tdp[pagina].modificada = 1;   // contarla y marcar la
        S->numrefsescritura ++;          // página como modificada
    }
    S->tdp[pagina].timestamp = S->reloj;            //Guardamos el valor del reloj en el campo timestamp de la pagina
    if (S->reloj == 4294967295) {
        printf("Error, desbordamiento del reloj");  //Controlamos el desbordamiento y si es asi reseteamos el reloj
        S->reloj = 0;                               // Se usan 4 bytes para almacenar un unsigned int en c por lo que el maximo es 2^32
    }
    S->reloj ++;
}

// Funciones que simulan el sistema operativo

void tratar_fallo_de_pagina (ssistema * S, unsigned dir_virtual)
{
    int pagina, victima, marco, ult;

    S->numfallospag ++;
    pagina = dir_virtual / S -> tampag ;

    if (S -> detallado ){
        printf ("@ ¡FALLO DE PÁGINA en P %d!\n" , pagina );
    }

    if (S -> listalibres != -1)             // Si hay marcos libres :
    {
        ult = S -> listalibres ;            // Último de la lista
        marco = S -> tdm [ ult ]. sig ;     // Tomar el sig . ( el 1 o )

        if ( marco == ult )                 // Si son el mismo , es que
        {
            S -> listalibres = -1;          // sólo quedaba uno libre
        }    

        else
        {
            S -> tdm [ ult ]. sig = S -> tdm [ marco ]. sig ; // Si no , puentear
        }

        ocupar_marco_libre (S , marco , pagina );
  
    }
    else
        {
            victima = elegir_pagina_para_reemplazo(S);

            reemplazar_pagina(S, victima, pagina);
        }
  
    // TODO: Teclear código que simula la respuesta del Sistema
    //       Operativo a un trap de fallo de página provocado
}

int elegir_pagina_para_reemplazo (ssistema * S)
{
    int marco, victima, pagina, min;
    min = 4294967295; //Se inicializa min con el valor maximo

    for (int i = 0; i<S->nummarcos; i++) { // Se van recorriendo todas las paginas hasta que encontramos la del menor timestamp
        pagina = S->tdm[i].pagina;
        if (S->tdp[pagina].timestamp < min) {
            min = S->tdp[pagina].timestamp;
            marco = i;
        }
    }

    victima = S->tdm[marco].pagina; // Se devuelve la pagina con el menor timestamp

    if (S->detallado)
        printf ("@ Eligiendo (LRU) P%d de M%d para "
                "reemplazarla\n", victima, marco);

    return victima;
}

void reemplazar_pagina (ssistema * S, int victima, int nueva)
{
    int marco;

    marco = S->tdp[victima].marco;

    if (S->tdp[victima].modificada)
    {
        if (S->detallado)
            printf ("@ Volcando P%d modificada a disco para "
                    "reemplazarla\n", victima);

        S->numescrpag ++;
    }

    if (S->detallado)
        printf ("@ Reemplazando víctima P%d por P%d en M%d\n",
                victima, nueva, marco);

    S->tdp[victima].presente = 0;

    S->tdp[nueva].presente = 1;
    S->tdp[nueva].marco = marco;
    S->tdp[nueva].modificada = 0;

    S->tdm[marco].pagina = nueva;
}

void ocupar_marco_libre (ssistema * S, int marco, int pagina)
{
    if (S->detallado)
        printf ("@ Alojando P%d en M%d\n", pagina, marco);

    S->tdp[pagina].marco = marco;
    S->tdp[pagina].presente = 1;
    S->tdm[marco].pagina = pagina;

}

// Funciones que muestran resultados

void mostrar_tabla_de_paginas (ssistema * S)
{
    int p;

    printf ("%10s %10s %10s %s %s\n",
            "PÁGINA", "Presente", "Marco", "Modificada", "Reloj");

    for (p=0; p<S->numpags; p++)
        if (S->tdp[p].presente)
            printf ("%8d   %6d     %8d   %6d  %6d\n", p, S->tdp[p].presente, S->tdp[p].marco, S->tdp[p].modificada, S->tdp[p].timestamp);
        else
            printf ("%8d   %6d     %8s   %6s %6s\n", p, S->tdp[p].presente, "-", "-", "-");
}

void mostrar_tabla_de_marcos (ssistema * S)
{
    int p, m;

    printf ("%10s %10s %10s   %s\n",
            "MARCO", "Página", "Presente", "Modificada");

    for (m=0; m<S->nummarcos; m++)
    {
        p = S->tdm[m].pagina;

        if (p==-1)
            printf ("%8d   %8s   %6s     %6s\n", m, "-", "-", "-");
        else if (S->tdp[p].presente)
            printf ("%8d   %8d   %6d     %6d\n", m, p,
                    S->tdp[p].presente, S->tdp[p].modificada);
        else
            printf ("%8d   %8d   %6d     %6s   ¡ERROR!\n", m, p,
                    S->tdp[p].presente, "-");
    }
}

void mostrar_informe_reemplazo (ssistema * S)
{
    printf ("Reemplazo LRU\n");
    int p, min, max = 0;
    min = 4294967295;

    for (p=0; p<S->numpags; p++) {
        if (S->tdp[p].presente) {
            printf ("Pagina: %8d  Reloj: %10d\n", p, S->tdp[p].timestamp);
            if (S->tdp[p].timestamp<min) {
                min = S->tdp[p].timestamp;
            }
            if (S->tdp[p].timestamp>max) {
                max = S->tdp[p].timestamp;
            }
        }  
    }
    printf ("Reloj minimo: %10d\n", min); 
    printf ("Reloj maximo: %10d\n", max);     
}


